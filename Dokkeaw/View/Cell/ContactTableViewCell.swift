//
//  ContactTableViewCell.swift
//  Dokkeaw
//
//  Created by Aom on 1/17/18.
//  Copyright © 2018 Aom. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {

    @IBOutlet weak var imgContact: UIImageView!
    
    @IBOutlet weak var lbNameCon: UILabel!
    
    @IBOutlet weak var txtPosition: UITextView!
    
    @IBOutlet weak var imgAbout: UIImageView!
    
    @IBOutlet weak var lbAbout: UILabel!
    
    
    @IBOutlet weak var lbPosition: UILabel!
    
    @IBOutlet weak var lbSecretary: UILabel!
    
    @IBOutlet weak var lbTel: UILabel!
    
}
