//
//  DetailTableViewCell.swift
//  Dokkeaw
//
//  Created by Aom on 1/25/18.
//  Copyright © 2018 Aom. All rights reserved.
//

import UIKit
import DKCarouselView

class DetailTableViewCell: UITableViewCell,UIScrollViewDelegate {
    
    @IBOutlet weak var txtTopic: UITextView!
    
    @IBOutlet weak var lbProvince: UILabel!
    
    @IBOutlet weak var lbDate: UILabel!
    
    @IBOutlet weak var txtDetail: UITextView!
    
    @IBOutlet weak var webView: UIWebView!
    
    @IBOutlet weak var carouselView: DKCarouselView!
    
}
