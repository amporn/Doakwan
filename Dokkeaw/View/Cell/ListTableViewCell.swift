//
//  ListTableViewCell.swift
//  Dokkeaw
//
//  Created by Aom on 1/24/18.
//  Copyright © 2018 Aom. All rights reserved.
//

import UIKit

class ListTableViewCell: UITableViewCell {

    @IBOutlet weak var imgList: UIImageView!
    
    @IBOutlet weak var txtList: UITextView!
    
    @IBOutlet weak var lbProvince: UILabel!
    
    @IBOutlet weak var lbDate: UILabel!
    
    
    @IBOutlet weak var lbTopic: UILabel!
    
}
