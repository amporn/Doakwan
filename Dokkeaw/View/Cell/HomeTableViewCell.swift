//
//  HomeTableViewCell.swift
//  Dokkeaw
//
//  Created by Aom on 1/31/18.
//  Copyright © 2018 Aom. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgHome: UIImageView!
    
    @IBOutlet weak var lbHome: UILabel!
    
    @IBOutlet weak var txtDetail: UITextView!
    
    @IBOutlet weak var webViewHome: UIWebView!
}
