//
//  MenuCollectionViewCell.swift
//  Rescue Alert
//
//  Created by Aom on 3/22/2560 BE.
//  Copyright © 2560 Aom. All rights reserved.
//

import UIKit

class MenuCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var viewTitle: UIView!
    
    @IBOutlet weak var imgFrame: UIImageView!
    
    @IBOutlet weak var btnClose: UIButton!
    
}
