//
//  KrongViewController.swift
//  Dokkeaw
//
//  Created by Aom on 3/9/18.
//  Copyright © 2018 Aom. All rights reserved.
//

import UIKit

class KrongViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    @IBOutlet weak var tbDetail: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return  1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
         let cell = tbDetail.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HomeTableViewCell
        
        cell.txtDetail.text = " “แก้วกระจ่างกลางใจราษฎร์มหาดไทย“ \n\n"  + "๐ โอ้ดอกแก้วแพรวพราวขาวบริสุทธิ์ \n งามประดุจเพชรน้ำค้างกระจ่างสี \n พรูสะพรั่งดุจพลังสามัคคี \n เบิกบานในใจน้องพี่มหาดไทย \n\n ๐ ชุมนุมช่อช้อยแฉล้มแย้มระยับ \n กลีบงุ้มงอนสลอนสลับสว่างไสว \n เปรียบไมตรีที่สร้างสรรค์อันอำไพ \n เทิดแม่บ้านมหาดไทยให้ไพบูลย์ \n\n ๐ หอมเอยหอมรวยรินกลิ่นดอกแก้ว \n กลีบร่วงแล้วยังหอมรินมิสิ้นสูญ \n ดุจความรักความผูกพันที่เพิ่มพูน \n ต่างช่วยเหลือเกื้อกูลกันและกัน \n\n ๐ แม้อยู่ห่างยังหอมตามมาเตือนจิต \n ชวนเชยชิดกลีบแก้วแพรวไพรสัณฑ์ \n อุปมาความห่วงใยใจผูกพัน \n เหล่าแม่บ้านไม่มีวันจะโรยรา \n\n ๐ ดอกแก้วงามทุกดอกที่ออกช่อ \n คือแรงก่อแรงกายอันแกร่งกล้า \n คือแรงเสริมแรงสร้างแรงศรัทธา \n คือแรงพัฒนาสมาคมให้มั่นคง \n\n  ๐ คือหน้าที่บำบัดทุกข์ที่ขุกเข็ญ \n ไทยร่มเย็นเป็นความฝันอันสูงส่ง \n ทั้งเทิดทูนคุณธรรมให้ดำรง \n ดุจกลิ่นแก้วหอมบรรจงไม่จืดจาง \n\n ๐ แก้วผการะย้าแย้มช่างแช่มช้อย \n ชวนถนอมกลีบแก้วน้อยไว้แนบข้าง \n เหมือนรากฐานครอบครัวต้องหมั่นวาง \n ให้ถูกทางทุกเขตคามด้วยความรัก \n\n ๐ กลีบดอกแก้วดุจมณีศรีสยาม \n คือคุณค่าสง่างามคือเกียรติศักดิ์ \n คือคุณธรรมความดีที่พร้อมพรัก \n คือสัญลักษณ์ของแม่บ้านมหาดไทย๚ \n\n นายจักรสิน พิเศษสาทร-ประพันธ์"
        
        return cell
    }
}
