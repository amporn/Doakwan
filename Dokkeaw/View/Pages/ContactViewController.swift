//
//  ViewController.swift
//  Doakwan
//
//  Created by Aom on 1/3/18.
//  Copyright © 2018 Aom. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage
import MBProgressHUD

class ContactViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource ,UICollectionViewDelegate,UICollectionViewDataSource {
    
    @IBOutlet weak var cellectMenu: UICollectionView!
    
    var loadprogress:MBProgressHUD = MBProgressHUD()
    
    let menuList = ["ที่ปรึกษา", "กรรมการบริหาร","กรรมการกลาง", "คณะทำงาน"]
    
    var menuSelected = 0
    
    var selectedIndex = Int ()
    
    @IBOutlet weak var tbContact: UITableView!
    
    var selectedCellIndexPath: NSIndexPath?
    let selectedCellHeight: CGFloat = 260
    let unselectedCellHeight: CGFloat = 160
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //          callService(type: menuSelected)
        shwProgress()
        delay(0.5) {
            self.tbContact.reloadData()
            self.cellectMenu.reloadData()
        }
        
    }
    
    func menuShow(menuIndex: Int) {
        
        if (menuIndex == 0) {
        } else {
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.callService(type: menuSelected)
        
    }
    
    func shwProgress() {
        
        loadprogress = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadprogress.mode = MBProgressHUDMode.indeterminate
        loadprogress.labelText = "Loading"
        loadprogress.show(animated: true)
        
        self.view.isUserInteractionEnabled = false
        
        self.delay(1.5) {
            self.view.isUserInteractionEnabled = true
            self.loadprogress.hide(animated: true)
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return  menuList.count
    }
    
    func collectionView(_ collectionView: UICollectionView,  cellForItemAt cellForItemAtindexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: cellForItemAtindexPath) as! MenuCollectionViewCell
        
        var listjson = menuList[cellForItemAtindexPath.row]
        cell.lbTitle.text = menuList[cellForItemAtindexPath.row]
        
        if selectedIndex == cellForItemAtindexPath.row
        {
//            UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1.0)
            cell.backgroundColor = UIColor(red: 228.0/255, green: 222.0/255, blue: 159.0/255, alpha: 1.0) //UIColor(red:0.228,green:0.222,blue:0.159,alpha:1.0)
        }
        else
        {
            cell.backgroundColor = UIColor.white
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedIndex = indexPath.row
        
        self.cellectMenu.reloadData()
        
        menuSelected = indexPath.row
        
        menuShow(menuIndex: menuSelected)
        
        shwProgress()
        
        jsonContact.removeAll()
        
         self.callService(type: self.menuSelected)
            
            delay(1) {
                
                self.tbContact.reloadData()
            }
        
    }
    
    func callService(type:Int){
        
        ConnectContact.serviceContact(typeCon:type, success:{(response) -> Void in
            
        }) {(message) -> Void in  }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return jsonContact.count
        //                return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:ContactTableViewCell = self.tbContact.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ContactTableViewCell
        
//        do  {
        
            var listjson = jsonContact[indexPath.row]
            
            if let fullName = listjson["fullName"].string {
                
                cell.lbNameCon.text = fullName
                
            }else {
                cell.lbNameCon.text = ""
                
            }
            
            if let postion = listjson["affiliation"].string {
                cell.txtPosition.text = postion
            }else {
                cell.txtPosition.text = ""
                
            }
            
            if let imgContact = listjson["attachFileNameOriginal"].string {
                
                var imagePath = apiDownload
                
                imagePath += imgContact
                
                let imgUrl = NSURL(string:imagePath)
                
                cell.imgContact.sd_setImage(with: imgUrl as! URL, placeholderImage:UIImage(named:"placeholder"))
                
            }else {
                cell.imgContact.image = UIImage(named:"icon_contact")
                cell.imgContact.contentMode = .scaleAspectFit
            }
            
            //        "secretaryFullName": "นางอรอุมา สินธุชัย",
            //        "secretaryTel": "08-8161-5655",
            
            if let secretaryTel = listjson["secretaryTel"].string {
                cell.lbTel.text = secretaryTel
            }else {
                cell.lbTel.text = ""
                
            }
//        }catch let error as NSError  {
//            print("Couldn't write to file: \(error.localizedDescription)")
//        }
        
    return cell
}

func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    var selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
    
    var listjson = jsonContact[indexPath.row]
    
    if let secretaryTel = listjson["secretaryTel"].string {
        
        if selectedCellIndexPath != nil || selectedCellIndexPath == indexPath as NSIndexPath {
            
            selectedCellIndexPath = nil
        } else if selectedCellIndexPath != nil {
            
            tbContact.scrollToRow(at: indexPath as IndexPath, at: .none, animated: true)
        }else {
            
            selectedCellIndexPath = indexPath as NSIndexPath
        }
        
        tbContact.beginUpdates()
        tbContact.endUpdates()
        
        selectedCell.contentView.backgroundColor = UIColor.gray
        
        print("secretaryTel if " ,secretaryTel)
    }else {
        print("secretaryTel else " )
        selectedCell.contentView.backgroundColor = UIColor.white
        
        //             tbContact.allowsSelection = false
        
    }
    
}

func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
    if selectedCellIndexPath == indexPath as NSIndexPath {
        
        return selectedCellHeight
    }
    
    return unselectedCellHeight
}

func delay(_ delay:Double, closure:@escaping ()->()) {
    
    let when = DispatchTime.now() + delay
    DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
}

func applicationDidReceiveMemoryWarning(application: UIApplication) {
    URLCache.shared.removeAllCachedResponses()
    print("contact applicationDidReceiveMemoryWarning")
}

override func didReceiveMemoryWarning() {
    
    print("contact didReceiveMemoryWarning")
}
}

