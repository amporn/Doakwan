
//
//  ListViewController.swift
//  Dokkeaw
//
//  Created by Aom on 1/24/18.
//  Copyright © 2018 Aom. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage
import MBProgressHUD

class ListViewController: UIViewController  , UITableViewDataSource , UITableViewDelegate {
    
    @IBOutlet weak var tbList: UITableView!
    
    var loadprogress:MBProgressHUD = MBProgressHUD()
    
    @IBOutlet weak var lbShwDate: UIButton!
    
    var tCurDate = ""
    
    var imgURL = NSURL(string:"")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let date = Date()
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "MMM YYYY"
        inputFormatter.dateFormat = "dd-MM-yyyy"
        inputFormatter.locale = Locale(identifier: "th_TH")
        tCurDate = inputFormatter.string(from: date)
        print("current date " ,tCurDate)
        
        shwProgress()
        
        clearQueryData()
        callService(startDate: "", endDate: "")
        
        //        delay(0.5) {
        //            self.tbList.reloadData()
        //        }
        
    }
    
    func clearQueryData() {
        
        jsonList.removeAll(keepingCapacity: false)
        self.tbList.reloadData()
    }
    
    func shwProgress() {
        
        loadprogress = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadprogress.mode = MBProgressHUDMode.indeterminate
        loadprogress.labelText = "Loading"
        loadprogress.show(animated: true)
        
        self.delay(0.75) {
            self.loadprogress.hide(animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        //        jsonList.removeAll()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        clearQueryData()
        
        if startDate == nil  {
            
            self.callService(startDate: "", endDate: "")
            //            self.lbShwDate.setTitle("  เลือกข้อมูลระหว่างวันที่ 08-02-2561 ถึง 08-02-2561 " , for: .normal)
            //            self.lbShwDate.alpha = 0.8
            print("viewDidAppear if")
            
        } else {
            
            //            delay(0.3) {
            
            self.callService(startDate:UserDefaults.standard.value(forKey: startDate!) as! String, endDate: UserDefaults.standard.value(forKey: endDate!) as! String)
            //                self.callService(startDate: "", endDate: "")
            //                self.lbShwDate.setTitle("  เลือกข้อมูลระหว่างวันที่  \(UserDefaults.standard.value(forKey: startDate!) as! String)  ถึง  \(UserDefaults.standard.value(forKey: endDate!) as! String)  " , for: .normal)
            //
            //                self.lbShwDate.alpha = 0.8
            
            //                print("viewDidAppear list startDate " ,UserDefaults.standard.value(forKey: startDate!) as! String)
            //        print("viewDidAppear list endDate " ,UserDefaults.standard.value(forKey: endDate!) as! String)
            
            print("viewDidAppear else ")
            
            //            }
            
        }
    }
    
    //    override func viewDidDisappear(_ animated: Bool) {
    //        super.viewDidDisappear(true)
    //
    //    }
    //
    //    override func viewWillDisappear(_ animated: Bool) {
    //        super.viewWillDisappear(true)
    //
    //        jsonList.removeAll()
    //    }
    
    func callService(startDate:String,endDate:String) {
        
        ConnectListNews.serviceList(startDate: startDate, endDate: endDate,success:{(response) -> Void in
            
            do {
            
//            let json = try? JSON(data:((response?.data))!)
                
                let json = try? JSON(data: (response as! NSData) as Data)
            
                DoakwanHelper.appenList(json: json!)
            
            self.tbList.reloadData()
                
            } catch {
                    
                }
            
            self.delay(0.5) {
                
                self.lbShwDate.alpha = 0
                
            }
               
            
        }) {(message) -> Void in  }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //        print("jsonList.count ",jsonList.count)
        
        //        if jsonList != nil {
        return jsonList.count
        //        }else {
        //            return 1
        //        }
        
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        
//        tableView.estimatedRowHeight = 380
//                tableView.rowHeight = UITableViewAutomaticDimension
//        return tableView.rowHeight
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tbList.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ListTableViewCell
        
        var listJson = jsonList[indexPath.row]
        
        if let imageData:Array<JSON> = listJson["imageList"].arrayValue {
            
            var imgIndex = imageData[0]
            
            //            for jsonData in imageData {
            
            if imgIndex["attachFileName"].string != nil {
                
                var imagePath = apiDownload
                
                imagePath += imgIndex["attachFileName"].string!
                
                imgURL = NSURL(string:imagePath)
              
                cell.imgList.sd_setImage(with: imgURL as! URL, placeholderImage:UIImage(named:"placeholder"))

            }
            
            //            }
        }else {
            
        }
        
        
//        let contentSize =  cell.imgList.sizeThatFits((cell.imgList.bounds.size))
//        var frame = cell.imgList.frame
//        frame.size.height = (contentSize.height)
//        cell.imgList.frame = frame
        
//        let aspectRatioTextViewConstraint = NSLayoutConstraint(item: cell.imgList, attribute: .height, relatedBy: .equal, toItem: cell.imgList, attribute: .width, multiplier: (cell.imgList.bounds.height)/(cell.imgList.bounds.width), constant: 1)
//        cell.imgList.addConstraint(aspectRatioTextViewConstraint)
        
        if let topic = listJson["topic"].string {
            //            cell.txtList.text = topic
            cell.lbTopic.text = topic
        }else {
            cell.lbTopic.text = ""
            //            cell.txtList.text = ""
        }
        
//                let contentSize3 =  cell.lbTopic.sizeThatFits((cell.lbTopic.bounds.size))
//                var frame3 = cell.lbTopic.frame
//                frame3.size.height = (contentSize3.height)
//                cell.lbTopic.frame = frame3
//
//                let aspectRatioTextViewConstraint3 = NSLayoutConstraint(item: cell.lbTopic, attribute: .height, relatedBy: .equal, toItem: cell.lbTopic, attribute: .width, multiplier: (cell.lbTopic.bounds.height)/(cell.lbTopic.bounds.width), constant: 1)
//                cell.lbTopic.addConstraint(aspectRatioTextViewConstraint3)
        
        if let provinceName = listJson["provinceName"].string {
            cell.lbProvince.text = provinceName
        }else{
            cell.lbProvince.text = ""
        }
        
        if let createdDateLabel = listJson["createdDateLabel"].string {
            cell.lbDate.text = createdDateLabel
        }else{
            cell.lbDate.text = ""
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Detail", bundle:nil)
        let detailVC: DetailViewController = storyboard.instantiateViewController(withIdentifier: "DetailViewControllerId") as! DetailViewController
        
        self.shwProgress()
        
        delay(0.5) {
            
            self.navigationController?.show(detailVC, sender: nil)
            
            var listJson = jsonList[indexPath.row]
            
            detailVC.jsonDel = listJson
            
        }
        
    }
    
    @IBAction func pressCalenda(_ sender: UIBarButtonItem) {
        
        let alert = UIAlertController(title: "เลือกการแสดงข้อมูล", message: "", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "แสดงข้อมูลล่าสุด", style: .default , handler:{ (UIAlertAction)in
            
            jsonList.removeAll()
            self.shwProgress()
            
            self.lbShwDate.setTitle("  แสดงข้อมูลล่าสุด " , for: .normal)
            self.lbShwDate.alpha = 0.8
            
            self.delay(1) {
                self.tbList.reloadData()
            }
            
            self.self.delay(0.3) {
                self.callService(startDate: "", endDate: "")
            }
        }))
        
        alert.addAction(UIAlertAction(title: "แสดงข้อมูลวันนี้", style: .default , handler:{ (UIAlertAction)in
            
            jsonList.removeAll()
            
            self.shwProgress()
            
            self.lbShwDate.alpha = 0.8
            
            self.lbShwDate.setTitle("  เลือกข้อมูลระหว่างวันที่ \(self.tCurDate) ถึง \(self.tCurDate) " , for: .normal)
            
            self.delay(1) {
                self.tbList.reloadData()
            }
            
            self.self.delay(0.3) {
                self.callService(startDate:self.tCurDate, endDate: self.tCurDate)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "แสดงข้อมูลเดือนนี้", style: .default , handler:{ (UIAlertAction)in
            
            //            01-02-2561&endDate=28-02-2561
            
            jsonList.removeAll()
            self.shwProgress()
            
            self.lbShwDate.alpha = 0.8
            
            self.lbShwDate.setTitle("  เลือกข้อมูลระหว่างวันที่ 01-03-2561 ถึง 31-03-2561 " , for: .normal)
            
            self.delay(1) {
                self.tbList.reloadData()
            }
            
            self.self.delay(0.3) {
                self.callService(startDate:"01-03-2561", endDate: "31-03-2561")
                
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: "เลือกวันแสดงข้อมูล", style: .default, handler:{ (UIAlertAction)in
            
            let storyboard = UIStoryboard(name: "Calenda", bundle:nil)
            let detailVC: CalendaViewController = storyboard.instantiateViewController(withIdentifier: "CalendaViewControllerId") as! CalendaViewController
            
            detailVC.modalTransitionStyle = .coverVertical
            detailVC.modalPresentationStyle = .overCurrentContext
            self.present(detailVC, animated: true, completion: {
                
            })
            
        }))
        
        alert.addAction(UIAlertAction(title: "ยกเลิก", style: .cancel, handler:{ (UIAlertAction)in
            
        }))
        
        self.present(alert, animated: true, completion: {
            
        })
    }
    
    func delay(_ delay:Double, closure:@escaping ()->()) {
        
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
    
    func applicationDidReceiveMemoryWarning(application: UIApplication) {
        URLCache.shared.removeAllCachedResponses()
        print("applicationDidReceiveMemoryWarning")
    }
    
    override func didReceiveMemoryWarning() {
        
        print("didReceiveMemoryWarning")
    }
}
