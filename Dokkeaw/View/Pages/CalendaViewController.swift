//
//  ViewController.swift
//  Koyomi
//
//  Created by shoheiyokoyama on 10/09/2016.
//  Copyright (c) 2016 shoheiyokoyama. All rights reserved.
//

import UIKit
import Koyomi

class CalendaViewController: UIViewController {
    
    var dateSelFrom: Date = Date()
    
    var dateSelTo:Date = Date()
    
     var dateCurrent:Date = Date()
    
    var resulftFrom = ""
    var resulfTo = ""
    
    @IBOutlet weak var btnFrom: UIButton!
    
    @IBOutlet weak var btnTo: UIButton!
    
    @IBOutlet fileprivate weak var koyomi: Koyomi! {
      
        didSet {
            
            koyomi.circularViewDiameter = 0.2
            koyomi.calendarDelegate = self
            koyomi.inset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            koyomi.weeks = ("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat")
            koyomi.style = .standard
            koyomi.dayPosition = .center
            koyomi.selectionMode = .sequence(style: .semicircleEdge)
            //             koyomi.selectionMode = .sequence(style: .line)
            //            koyomi.selectedStyleColor = UIColor(red: 203/255, green: 119/255, blue: 223/255, alpha: 1)
            koyomi.selectedStyleColor = .blue
            koyomi
                .setDayFont(size: 14)
                .setWeekFont(size: 10)
            .setDayColor(.red, of: dateCurrent)
//            .setDayBackgrondColor(.red, of: dateSelFrom, to: dateSelFrom)
        }
    }
    
    @IBOutlet fileprivate weak var currentDateLabel: UILabel!
    
    fileprivate let invalidPeriodLength = 90
    
    @IBOutlet fileprivate weak var segmentedControl: UISegmentedControl! {
    
        didSet {
            segmentedControl.setTitle("Previous", forSegmentAt: 0)
            segmentedControl.setTitle("Current", forSegmentAt: 1)
            segmentedControl.setTitle("Next", forSegmentAt: 2)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        currentDateLabel.text = koyomi.currentDateString()
        
//        resulftFrom = "08-02-2561"
//        resulfTo = "08-02-2561"
        
        UserDefaults.standard.setValue(resulftFrom , forKey: startDate!)
        
        UserDefaults.standard.setValue(resulfTo , forKey: endDate!)
        
        print("calenda viewDidLoad startDate ",UserDefaults.standard.value(forKey: startDate!))
        print("calenda viewDidLoad endDate ",UserDefaults.standard.value(forKey: endDate!))
        
        btnFrom.isSelected = true
//
         print("btnFrom.isSelected  ",btnFrom.isSelected)
        
    }
    
    fileprivate func date(_ date: Date, later: Int) -> Date {
        var components = DateComponents()
        components.day = later
        return (Calendar.current as NSCalendar).date(byAdding: components, to: date, options: NSCalendar.Options(rawValue: 0)) ?? date
    }
    
    func delay(_ delay:Double, closure:@escaping ()->()) {
        
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
    
    @IBAction func pressOK(_ sender: UIButton) {
        
        UserDefaults.standard.setValue(resulftFrom , forKey: startDate!)
        
        UserDefaults.standard.setValue(resulfTo , forKey: endDate!)
        
        print("press OK resulftFrom ",resulftFrom)
        
        print("press OK resulfTo ",resulfTo)
        
        print("btnFrom.isSelected pressOK ",btnFrom.isSelected)
    }
    
    @IBAction func pressCancel(_ sender: UIButton) {
        
        dismiss(animated: true, completion: nil)
        
        let storyboard = UIStoryboard(name: "List", bundle:nil)
        let detailVC: ListViewController = storyboard.instantiateViewController(withIdentifier: "ListViewControllerId") as! ListViewController
        navigationController?.show(detailVC, sender: nil)
    }
    
    
    @IBAction func pressFrom(_ sender: UIButton) {
        
        btnFrom.backgroundColor = UIColor.darkGray
        btnTo.backgroundColor = UIColor.white
//
//        let inputFormatter = DateFormatter()
//        inputFormatter.dateFormat = "MMM YYYY"
//        inputFormatter.dateFormat = "dd-MM-yyyy"
//        inputFormatter.locale = Locale(identifier: "th_TH")
//        resulftFrom = inputFormatter.string(from: dateSelFrom)
//
//        print("press From resulftFrom ",resulftFrom)
        
        if btnFrom.isSelected == true {
//            btnFrom.isSelected = false
            btnFrom.isEnabled = false
        }else {
            btnFrom.isSelected = true
        }
    
        print("btnFrom.isSelected pressFrom ",btnFrom.isSelected)
    }
    
    
    @IBAction func pressTo(_ sender: UIButton) {
        
        btnTo.backgroundColor = UIColor.darkGray
        btnFrom.backgroundColor = UIColor.white
        
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "MMM YYYY"
        inputFormatter.dateFormat = "dd-MM-yyyy"
        inputFormatter.locale = Locale(identifier: "th_TH")
        resulfTo = inputFormatter.string(from: dateSelTo)
        
          btnFrom.isEnabled = true
        
    }
    
}

extension CalendaViewController {
    @IBAction func tappedControl(_ sender: UISegmentedControl) {
        let month: MonthType = {
            switch sender.selectedSegmentIndex {
            case 0:  return .previous
            case 1:  return .current
            default: return .next
            }
        }()
        koyomi.display(in: month)
    }
    
    func configureStyle(_ style: KoyomiStyle) {
        
        koyomi.style = style
        koyomi.reloadData()
    }
}

extension CalendaViewController: KoyomiDelegate {
    
    func koyomi(_ koyomi: Koyomi, didSelect date: Date?, forItemAt indexPath: IndexPath) {
        
        if btnFrom.isEnabled == true {
        
            dateSelFrom = date!
            
            let inputFormatter = DateFormatter()
            inputFormatter.dateFormat = "MM/dd/yyyy"
            //                date = inputFormatter.date(from: dateString)
            inputFormatter.dateFormat = "dd-MM-yyyy"         //"MMM YYYY"                ///"yyyy-MMM-dd"
            //        inputFormatter.locale = Locale(identifier: "th_TH")
            resulftFrom = inputFormatter.string(from: date!)
          
//            print("date didSelect ", resultString)
            print("You Selected: \(dateSelFrom)")
            
            currentDateLabel.text = resulftFrom
        }
        
    }
    
    func koyomi(_ koyomi: Koyomi, currentDateString dateString: String) {
        
        currentDateLabel.text = dateString
        
    }
    
    @objc(koyomi:shouldSelectDates:to:withPeriodLength:)
  
    func koyomi(_ koyomi: Koyomi, shouldSelectDates date: Date?, to toDate: Date?, withPeriodLength length: Int) -> Bool {
        
        if length > invalidPeriodLength {
            
            print("More than \(invalidPeriodLength) days are invalid period.")
            
            return false
        }
        return true
    }
}

