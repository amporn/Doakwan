//
//  AboutViewController.swift
//  Dokkeaw
//
//  Created by Aom on 1/23/18.
//  Copyright © 2018 Aom. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    var titles = ["Contact us" , "Like us on Facebook"]

    var images = ["icon_mail","icon_fb"]
    
    @IBOutlet weak var tableAbout: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableAbout.reloadData()

    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        return titles.count
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableAbout.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ContactTableViewCell
        
        cell.lbAbout.text = self.titles[indexPath.row]
        
        let imageName = self.images[indexPath.row]
        cell.imgAbout.image = UIImage(named:imageName)
        
        return cell
        
    }
   
}
