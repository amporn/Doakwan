//
//  MapViewController.swift
//  Doakwan
//
//  Created by Aom on 1/3/18.
//  Copyright © 2018 Aom. All rights reserved.
//

import UIKit
import SwiftyJSON
import MBProgressHUD
import CoreLocation

class MapViewController: UIViewController ,CLLocationManagerDelegate {
    
    @IBOutlet weak var mapView: GMSMapView!
    
    let locationManager = CLLocationManager()
    var currentLoaction : CLLocation? // class MapViewController  initializers
    var marker : GMSMarker = GMSMarker()
    
    var loadprogress:MBProgressHUD = MBProgressHUD()
    
    @IBOutlet weak var btnShwDate: UIButton!
    
    var tCurDate = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configMap()
        
        mapView.clear()
        
        shwProgress()
        
        self.callService(startDate: "", endDate: "")
        
        let date = Date()
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "MMM YYYY"
        inputFormatter.dateFormat = "dd-MM-yyyy"
        inputFormatter.locale = Locale(identifier: "th_TH")
        tCurDate = inputFormatter.string(from: date)
        print("current date " ,tCurDate)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
            self.callService(startDate: "", endDate: "")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        
        print("view DidDisappear")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
          print("view WillDisappear")
        
         self.callService(startDate: "", endDate: "")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
          print("view DidAppear")
    }
    
    func configMap() {
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
//        locationManager.startUpdatingLocation()
        
        if CLLocationManager.locationServicesEnabled() {
            //locationManager.startUpdatingHeading()
            locationManager.startUpdatingLocation()
        }
        
//        let rect = CGRect(x: 0, y: 0, width: mapView.frame.width, height: mapView.frame.height)
//
//        let camera : GMSCameraPosition = GMSCameraPosition.camera(withLatitude: 13.7539800, longitude: 100.5014400, zoom: 6)
//        mapView = GMSMapView.map(withFrame: rect, camera: camera)
//
//        mapView.camera = camera
        
        mapView.isMyLocationEnabled = true
        
        self.mapView.accessibilityElementsHidden = false
        mapView.settings.myLocationButton = true
        mapView.settings.compassButton = true
//        mapView.settings.c
        
        mapView.mapType = .normal
        
        self.mapView.delegate = self as! GMSMapViewDelegate
//        locationManager.cu
        
    }
    
    func callService(startDate:String,endDate:String){
        
        ConnectMap.serciveMap(startDate: startDate, endDate: endDate, success: {(response) -> Void in
            
            self.anotationMap()
            
        }) {(message) -> Void in
            
        }
    }
    
    func anotationMap() {
        
        var i = 0
        
        for list in jsonMap {
            
            //            "latitude": "16.8263285",
            //            "longitude": "100.25985259999993", longitude
            guard let latitude = list["latitude"].string else { return }
            guard let longitude = list["longitude"].string else { return }
            guard let provinceCode = list["provinceCode"].string else { return }
            guard let topic = list["topic"].string else { return }
            
            print("anotationMap latitude ",latitude)
              print("anotationMap longitude ",longitude)
            
            if latitude != "-" && longitude != "" {
                
                self.getLatLong(latitude: latitude, longitude: longitude,typePin: Int(provinceCode)!,title: topic, index:i)
         
            }
            
            i += 1
            
        }
        
    }
    
    func getLatLong(latitude:String,longitude:String,typePin:Int,title:String,index:Int) {
        
        let lat = Double(latitude)
        let lng = Double(longitude)
        
        let marker:GMSMarker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(lat!, lng!)
//        marker.title = title
        marker.icon = markerIcon(type: typePin, isColor: true, isBw: true)
        marker.snippet = "\(index)"
        
        marker.map = self.mapView
        
    }
    
    func markerIcon(type:Int,isColor:Bool,isBw:Bool) -> UIImage {
        
        var iconPin = "ic_marker"
        
        iconPin += String(type)
        
        if isBw {
            
            let originalImg = self.imageWithImage(image: UIImage(named: iconPin)!, scaledToSize: CGSize(width: 25.0, height: 25.0))
            
            let cifilterName = "CIMinimumComponent"
            
            let ciContext = CIContext(options:nil)
            let startImg = CIImage(image:originalImg)
            
            let filter = CIFilter(name:cifilterName)
            filter!.setDefaults()
            
            filter!.setValue(startImg, forKey: kCIInputImageKey)
            let filteredImgData = filter!.value(forKey: kCIInputImageKey) as! CIImage
            let filteredImgRef = ciContext.createCGImage(filteredImgData, from: filteredImgData.extent)
            
            return UIImage(cgImage:filteredImgRef!)
            
        }
        return UIImage(named:iconPin)!
    }
    
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        
        image.draw(in: CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: newSize.width, height: newSize.height))  )
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        //        newImage.imageOrientation = .
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    func delay(_ delay:Double, closure:@escaping ()->()) {
        
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
    
    func shwProgress() {
        
        loadprogress = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadprogress.mode = MBProgressHUDMode.indeterminate
        loadprogress.labelText = "Loading"
        loadprogress.show(animated: true)
        
        jsonMap.removeAll()
        self.mapView.clear()
        
        self.delay(1.5) {
            self.loadprogress.hide(animated: true)
        }
    }
    
    @IBAction func pressCalenda(_ sender: UIBarButtonItem) {
        
        let alert = UIAlertController(title: "เลือกการแสดงข้อมูล", message: "", preferredStyle: .actionSheet)
        
//        alert.addAction(UIAlertAction(title: "แสดงข้อมูลล่าสุด", style: .default , handler:{ (UIAlertAction)in
//
//            self.shwProgress()
//
//            self.btnShwDate.setTitle("  แสดงข้อมูลล่าสุด " , for: .normal)
//            self.btnShwDate.alpha = 0.8
//
//            self.callService(startDate:"", endDate: "")
//
//            self.anotationMap()
//
//            self.callService(startDate:"", endDate: "")
//
//            self.mapView.delegate = self
//
//            self.self.delay(2) {
//
//                self.btnShwDate.alpha = 0
//            }
//
//        }))
        
        alert.addAction(UIAlertAction(title: "แสดงข้อมูลวันนี้", style: .default , handler:{ (UIAlertAction)in
            
            self.shwProgress()
            
              self.btnShwDate.setTitle("  เลือกข้อมูลระหว่างวันที่ \(self.tCurDate) ถึง \(self.tCurDate) " , for: .normal)
            self.btnShwDate.alpha = 0.8
          
                self.callService(startDate:self.tCurDate, endDate: self.tCurDate)
            
                   self.anotationMap()
            
                  self.callService(startDate:self.tCurDate, endDate: self.tCurDate)
            
                print("jsonMap แสดงข้อมูลวันนี้  ",jsonMap)
            
            self.mapView.delegate = self
            
            self.self.delay(2) {
                
                self.btnShwDate.alpha = 0
            }
            

        }))
        
        alert.addAction(UIAlertAction(title: "แสดงข้อมูลเดือนนี้", style: .default , handler:{ (UIAlertAction)in
            
            self.shwProgress()
            
            self.btnShwDate.setTitle("  เลือกข้อมูลระหว่างวันที่ 01-03-2561 ถึง 31-03-2561 " , for: .normal)
            self.btnShwDate.alpha = 0.8
            
            self.callService(startDate:"01-03-2561", endDate: "31-03-2561")
            
            self.anotationMap()
            
            self.callService(startDate:"01-03-2561", endDate: "31-03-2561")
            
            self.mapView.delegate = self
            
            self.self.delay(2) {
                
                self.btnShwDate.alpha = 0
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: "เลือกวันแสดงข้อมูล", style: .default, handler:{ (UIAlertAction)in
         
            let storyboard = UIStoryboard(name: "CalendaMap", bundle:nil)
            let detailVC: CalendaMapViewController = storyboard.instantiateViewController(withIdentifier: "CalendaMapViewControllerId") as! CalendaMapViewController
            //
            detailVC.modalTransitionStyle = .coverVertical
            detailVC.modalPresentationStyle = .overCurrentContext
            self.present(detailVC, animated: true, completion: {
                
            })
            
        }))
        
        alert.addAction(UIAlertAction(title: "ยกเลิก", style: .cancel, handler:{ (UIAlertAction)in
            
        }))
        
        self.present(alert, animated: true, completion: {
            
        })
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
     
        let userLocation:CLLocation = locations[0] as CLLocation
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        //manager.stopUpdatingLocation()
        
        let center = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
//        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
           let camera : GMSCameraPosition = GMSCameraPosition.camera(withLatitude: 13.7539800, longitude: 100.5014400, zoom: 6)
        
//        mapView.setRegion(camera, animated: true)
        
            mapView.camera = camera
        
//        // Drop a pin at user's Current Location
//        let myAnnotation: MKPointAnnotation = MKPointAnnotation()
//        myAnnotation.coordinate = CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude);
//        myAnnotation.title = "Current location"
//        mapView.addAnnotation(myAnnotation)
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError)
    {
        print("Error \(error)")
    }
    
    
}

extension MapViewController:GMSMapViewDelegate  {
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        
//        let resources = (Bundle.main.loadNibNamed("InfoWindow", owner: self, options:nil) as Array<Any>!)
//
//        let optionalInfoWindow = resources?.first as! InfoWindow?
//
//        let indexRes = Int(marker.snippet!)
//
//        let listJson = jsonMapRescue[Int(indexRes!)]
        
        let resources = (Bundle.main.loadNibNamed("MapView", owner: self, options: nil))
        let optionInfoWindow = resources?.first as! MapView?
        let indexMap = Int(marker.snippet!)
        let listJson = jsonMap[indexMap!]
        
        if let disasterType = listJson["topic"].string {
            optionInfoWindow?.lbTitle.text = disasterType
        } else {
            optionInfoWindow?.lbTitle.text = ""
        }
       
        return optionInfoWindow
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {  //click newviewdetail
        
        let storyboard = UIStoryboard(name: "Detail", bundle:nil)
        let detailVC: DetailViewController = storyboard.instantiateViewController(withIdentifier: "DetailViewControllerId") as! DetailViewController
        
        delay(0.1) {
            self.shwProgress()
            self.navigationController?.show(detailVC, sender: nil)
        }
        
        let index = Int(marker.snippet!)
        
        var listJson = jsonMap[index!]
        
         detailVC.jsonDel = listJson
       
    }
    
//    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
//        print("Error \(error)")
//    }
    
}
