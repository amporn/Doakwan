//
//  DetailViewController.swift
//  Dokkeaw
//
//  Created by Aom on 1/25/18.
//  Copyright © 2018 Aom. All rights reserved.
//

import UIKit
import SwiftyJSON
import PhotoSlider
import DKCarouselView
import MBProgressHUD

class DetailViewController: UIViewController ,UITableViewDelegate ,UITableViewDataSource {
    
    @IBOutlet weak var tbDetail: UITableView!
    
    var cell:DetailTableViewCell = DetailTableViewCell()
    
    var loadprogress:MBProgressHUD = MBProgressHUD()
    
    var jsonDel = JSON()
    
    var imagesURL:[NSURL] = []
    
    var canClick = false
    
    var imageItems = [DKCarouselURLItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        shwProgress()
        
        print("detail jsondel " , jsonDel)
        
        //        delay(0.5) {
        self.tbDetail.reloadData()
        //        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tbDetail.reloadData()
    }
    
    func shwProgress() {
        
        loadprogress = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadprogress.mode = MBProgressHUDMode.indeterminate
        loadprogress.labelText = "Loading"
        loadprogress.show(animated: true)
        
        self.delay(1) {
            self.loadprogress.hide(animated: true)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        cell = tbDetail.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DetailTableViewCell
        
        if let imageData:Array<JSON> = jsonDel["imageList"].arrayValue {
            
            for jsonData in imageData {
                
                if jsonData["attachFileName"].string != nil {
                    
                    var imagePath = apiDownload
                    
                    imagePath += jsonData["attachFileName"].string!
                    
                    let imURL = NSURL(string: imagePath)!
                    self.imagesURL.append(imURL)
                    
                    let urlAD = DKCarouselURLItem.init()
                    urlAD.imageUrl = imagePath
                    
                    imageItems.append(urlAD)
                    canClick = true
                    
                    print("select imagePath" ,imagePath)
                    
                    if imageItems.isEmpty {
                        
                        let urlAD = DKCarouselURLItem.init()
                        let imagePath = ""
                        urlAD.imageUrl = imagePath
                        imageItems.append(urlAD)
                        canClick = false
                    }
                    
                    cell.carouselView.indicatorTintColor = UIColor.white
                    cell.carouselView.isFinite = false
                    //                    cell.carouselView.defaultImage = UIImage(named: "icon_banner")
                    cell.carouselView.setItems(imageItems)
                    
                    cell.carouselView.setDidSelect{ (item: DKCarouselItem!, index:Int) -> Void in
                        
                        print("select ")
                        
                        if self.canClick {
                            
                            print("select \(index)")
                            
                            let slider = PhotoSlider.ViewController(imageURLs: self.imagesURL as [URL])
                            slider.modalPresentationStyle = .fullScreen
                            //                            slider.modalTransitionStyle = .flipHorizontal
                            slider.currentPage = index
                          
                            self.present(slider, animated: true, completion: nil)
                            
//                            let fileManager = FileManager.default
//
//                            //get the image path
//                            let imagePath = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(urlAD.imageUrl)
                            
                            //get the image we took with camera
//                            let image = imageView.image!
//
//                            //get the PNG data for this image
//                            let data = UIImagePNGRepresentation(image)
                            
                            //store it in the document directory
//                            fileManager.createFile(atPath: imagePath as String, contents: data, attributes: nil)
                        }
                            
                        }
                        
                    }
                    
                }
                
            }
            
//        }
        
        
        //        let contentSize =  cell.webView.sizeThatFits((cell.webView.bounds.size))
        //        var frame = cell.webView.frame
        //        frame.size.height = (contentSize.height)
        //        cell.webView.frame = frame
        //
        //        let aspectRatioTextViewConstraint = NSLayoutConstraint(item: cell.webView, attribute: .height, relatedBy: .equal, toItem: cell.webView, attribute: .width, multiplier: (cell.webView.bounds.height)/(cell.webView.bounds.width), constant: 1)
        //        cell.webView.addConstraint(aspectRatioTextViewConstraint)
        //
        //        cell.webView.scalesPageToFit = true
        //        cell.webView.contentMode = .scaleAspectFit
        
        
        if let topic = jsonDel["topic"].string {
            cell.txtTopic.text = topic
        }else {
            cell.txtTopic.text = ""
        }
        
        let contentSize3 =  cell.txtTopic.sizeThatFits((cell.txtTopic.bounds.size))
        var frame3 = cell.txtTopic.frame
        frame3.size.height = (contentSize3.height)
        cell.txtTopic.frame = frame3
//
        let aspectRatioTextViewConstraint3 = NSLayoutConstraint(item: cell.txtTopic, attribute: .height, relatedBy: .equal, toItem: cell.txtTopic, attribute: .width, multiplier: (cell.txtTopic.bounds.height)/(cell.txtTopic.bounds.width), constant: 1)
        cell.txtTopic.addConstraint(aspectRatioTextViewConstraint3)
        
        if let provinceName = jsonDel["provinceName"].string {
            cell.lbProvince.text = provinceName
            print("provinceName del ",provinceName)
        }else{
            cell.lbProvince.text = ""
        }
        
        if let createdDateLabel = jsonDel["createdDateLabel"].string {
            cell.lbDate.text = createdDateLabel
            print("createdDateLabel del ",createdDateLabel)
        }else{
            cell.lbDate.text = ""
        }
        
        if var detail = jsonDel["detail"].string {
            
            var html = "<head>"
            html += "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
            html += "<style> body { font-size: 120%; } </style>"
            html += "</head>"
            html += "<body>"
            html += detail
            html += "</body>"
            html += "</html>"
            
            cell.webView.loadHTMLString(html, baseURL: nil)
        }else {
            //                        detailVC.tDetail = ""
        }
        
        return cell
    }
    
    func delay(_ delay:Double, closure:@escaping ()->()) {
        
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
}


