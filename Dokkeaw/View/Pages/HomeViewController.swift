//
//  HomeViewController.swift
//  Dokkeaw
//
//  Created by Aom on 1/30/18.
//  Copyright © 2018 Aom. All rights reserved.
//

import UIKit
import SwiftyJSON
import MBProgressHUD
import SDWebImage

class HomeViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    @IBOutlet weak var tbHome: UITableView!
    
    let menuList = ["ข้อบังคับของสมาคมแม่บ้านมหาดไทย พ.ศ. 2560", "กลอนดอกแก้วสมาคมแม่บ้านมหาด","เพลงมาร์ชสมาคมแม่บ้านมหาด", ""]
    //    var title: = ["ข้อบังคับของสมาคมแม่บ้านมหาดไทย พ.ศ. 2560" , "กลอนดอกแก้วสมาคมแม่บ้านมหาด" , "เพลงมาร์ชสมาคมแม่บ้านมหาด" , ""]
    
    var loadprogress:MBProgressHUD = MBProgressHUD()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delay(1) {
            self.tbHome.reloadData()
        }
        shwProgress()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        jsonContent.removeAll()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        callService()
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return jsonContent.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tbHome.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HomeTableViewCell
        
        var listJson = jsonContent[indexPath.row]
        
        //        if let title = listJson["title"].string {
        //            cell.lbHome.text = title
        //        }else{
        //            cell.lbHome.text = ""
        //        }
        
        cell.lbHome.text = menuList[indexPath.row]
        
        if let imageCover = listJson["imageCover"].string {
            
            var imagePath = apiDownload
            
            imagePath += imageCover
            
            var imgURL = URL(string:imagePath)
            
            cell.imgHome.sd_setImage(with: imgURL as! URL, placeholderImage:UIImage(named:"placeholder"))
            
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "HomeDetail1", bundle:nil)
        let detailVC: HomeDel1ViewController = storyboard.instantiateViewController(withIdentifier: "HomeDel1ViewControllerId") as! HomeDel1ViewController
        
        if indexPath.row == 0 {
            
            detailVC.tUrl = "http://www.dokkaew.moi.go.th/MOT_DK/public/aboutDK.do"
            navigationController?.show(detailVC, sender: nil)
            
        }else if indexPath.row == 1 {
            
            
            let storyboard = UIStoryboard(name: "HomeDetail", bundle:nil)
            let detailVC: KrongViewController = storyboard.instantiateViewController(withIdentifier: "KrongViewControllerId") as! KrongViewController
            
            navigationController?.show(detailVC, sender: nil)
            
        }else if indexPath.row == 2 {
            
            detailVC.tUrl = "http://www.dokkaew.moi.go.th/MOT_DK/public/intoForMobile.jsp"
            navigationController?.show(detailVC, sender: nil)
        }else {
            
        }
        
    }
    
    //    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    //    {
    //        let tappedImage = tapGestureRecognizer.view as! UIImageView
    //
    //        print("click image")
    //
    //        let browser = SKPhotoBrowser(photos: images1)
    //        present(browser, animated: true, completion:  nil)
    //
    //    }
    
    func shwProgress() {
        
        loadprogress = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadprogress.mode = MBProgressHUDMode.indeterminate
        loadprogress.labelText = "Loading"
        loadprogress.show(animated: true)
        
        self.delay(1.5) {
            self.loadprogress.hide(animated: true)
        }
    }
    
    func callService() {
        
        ConnectContent.serviceContent(success:{(response) -> Void in
            
        }) {(message) -> Void in  }
    }
    
    func delay(_ delay:Double, closure:@escaping ()->()) {
        
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
    
    
}
