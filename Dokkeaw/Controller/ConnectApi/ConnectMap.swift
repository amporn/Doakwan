//
//  ConnectMap.swift
//  Doakwan
//
//  Created by Aom on 1/15/18.
//  Copyright © 2018 Aom. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ConnectMap {
    
    class func serciveMap(startDate:String,endDate:String,success: @escaping successHandler,fail: @escaping failHandler) {
        
        let parameMap : [String:AnyObject] = ["cmd":"listNews" as AnyObject,"startDate":startDate as AnyObject,"endDate":endDate as AnyObject]
         
        Alamofire.request(apiDoakwan, method: .post, parameters:parameMap).responseJSON{ response in

            do {
                
                let json = try? JSON(data:(response.data)!)
                
                if let isOK:Bool = json!["ok"].boolValue {
                    
                    if isOK == true {
                        
                        success(response.data as AnyObject)
                        appendMap(json: json!)
                    }
                }else {
                    fail("fail")
                }
                
            }catch {
                
            }

        }
        
    }
    
    class func appendMap(json:JSON) {
    
        if let listJson:Array<JSON> = json["data"].arrayValue{
            
            for jsonData in listJson {
                
                jsonMap.append(jsonData)
//
//                print("response.data json " , jsonMap)
            }
            
        }
    }
    
}
