//
//  ConnectListNews.swift
//  Dokkeaw
//
//  Created by Aom on 1/24/18.
//  Copyright © 2018 Aom. All rights reserved.
//   122.155.13.198/MOT_DK/mobile?cmd=listNews

import UIKit
import Alamofire
import SwiftyJSON

class ConnectListNews:NSObject   {
    
    class func serviceList(startDate:String,endDate:String,success:@escaping successHandler, fail:@escaping failHandler) {
        
//        122.155.13.198/MOT_DK/mobile?cmd=listNews&startDate=26-01-2561&endDate=26-01-2561
        
        let parameList : [String:AnyObject] = ["cmd":"listNews" as AnyObject,"startDate":startDate as AnyObject,"endDate":endDate as AnyObject]
        
        Alamofire.request(apiDoakwan, method: .post, parameters: parameList).responseJSON { response in
            
            print("response.request ",response.request)
            
            do {
           
                let json = try? JSON(data:(response.data)!)
            
                if let isOK:Bool = json!["ok"].boolValue {
                    
                    if isOK == true {
                        
                        success(response.data as AnyObject)
//                        appenList(json: json!)
                    }
                }else {
                    fail("Fail")
                }
                
            }catch {
                
            }
            
        }
    }
    
//    class func appenList(json:JSON) {
//
//        if let listJson:Array<JSON> = json["data"].arrayValue {
//
//            for jsonData in listJson {
//
//                jsonList.append(jsonData)
//
////                print("json jsonList ",jsonList)
//
//            }
//        }
//    }
}
