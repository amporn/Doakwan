//
//  TableViewCell.swift
//  Dokkeaw
//
//  Created by Aom on 1/17/18.
//  Copyright © 2018 Aom. All rights reserved.
//        //        122.155.13.198/MOT_DK/mobile?cmd=listContact&typeContact=1

import UIKit
import Alamofire
import SwiftyJSON

class ConnectContact {
    
    class func serviceContact(typeCon:Int,success:@escaping successHandler, fail:@escaping failHandler) {

        let parameCon : [String:AnyObject] = ["cmd":"listContact" as AnyObject,"typeContact":typeCon as AnyObject]
        
        Alamofire.request(apiDoakwan, method: .post, parameters: parameCon).responseJSON { response in
            
            do {
                let json = try? JSON(data:(response.data)!)
                if let isOK:Bool = json!["ok"].boolValue {
                    
                    if isOK == true {
                        success(response.data as AnyObject)
                        appenContact(json: json!)
                    }
                }else {
                    fail("fail")
                }
                
            }catch {
                
            }
            
        }
    }
    
    class func appenContact(json:JSON) {
        
        if let listJson:Array<JSON> = json["data"].arrayValue {
            
            for jsonData in listJson {
                
                jsonContact.append(jsonData)
                
//                print("json contact type ",jsonContact)
                
            }
        }
    }
    
}
