//
//  ConnectConnect.swift
//  Dokkeaw
//
//  Created by Aom on 1/31/18.
//  Copyright © 2018 Aom. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class ConnectContent  {
    
    class func serviceContent(success:@escaping successHandler, fail:@escaping failHandler) {
        
        let parameCon : [String:AnyObject] = ["cmd":"listContent" as AnyObject]
        
        Alamofire.request(apiDoakwan, method: .post, parameters: parameCon).responseJSON { response in
            
            do {
                
                let json = try? JSON(data:(response.data)!)
                
                if let isOK:Bool = json!["ok"].boolValue {
                    
                    if isOK == true {
                        success(response.data as AnyObject)
                        appenContent(json: json!)
                    }
                }else {
                    fail("fail")
                }
                
            }catch {
                
            }
            
        }
    }
    
    class func appenContent(json:JSON) {
        
        if let listJson:Array<JSON> = json["data"].arrayValue {
            
            for jsonData in listJson {
                
                jsonContent.append(jsonData)
                
                print("jsonContent ",jsonContent)
                
            }
        }
    }
    
}

